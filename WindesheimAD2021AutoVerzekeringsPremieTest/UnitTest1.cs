using System;
using System.Security.Authentication.ExtendedProtection;
using Moq;
using WindesheimAD2021AutoVerzekeringsPremie;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremieTest
{
    public class UnitTest1
    {
        [Fact]
        public void WACalculationsAreCorrect()
        {
            //setup and calculate
            var vehicle = new Vehicle(120, 5000, 2011);
            var actual = PremiumCalculation.CalculateBasePremium(vehicle);

            Assert.Equal(48, actual);
        }

        [Fact]
        void AgeUnder23Get15PercentHigherPrice()
        {
            //Create data
            var firstHolder = new PolicyHolder(22, "01-01-2015", 8000, 0);
            var secondHolder = new PolicyHolder(23, "01-01-2015", 8000, 0);
            var vehicle = new Vehicle(50, 1000, 2000);
            var coverage = InsuranceCoverage.WA;

            //Calculate
            var firstCalculation = new PremiumCalculation(vehicle, firstHolder, coverage);
            var secondCalculation = new PremiumCalculation(vehicle, secondHolder, coverage);

            //Check if price as expected
            Assert.Equal(secondCalculation.PremiumAmountPerYear * 1.15, firstCalculation.PremiumAmountPerYear);
        }

        [Fact]
        void LessThan5yLicenceGet15PercentHigherPrice()
        {
            //Create data
            var firstHolder = new PolicyHolder(25, DateTime.Today.AddYears(-5).ToString("dd-MM-yyyy"), 8000, 0);
            var secondHolder = new PolicyHolder(25, DateTime.Today.AddYears(-5).AddDays(1).ToString("dd-MM-yyyy"), 8000, 0);
            var vehicle = new Vehicle(50, 1000, 2000);
            var coverage = InsuranceCoverage.WA;

            //Calculate
            var firstCalculation = new PremiumCalculation(vehicle, firstHolder, coverage);
            var secondCalculation = new PremiumCalculation(vehicle, secondHolder, coverage);

            //Check if price as expected
            Assert.Equal(secondCalculation.PremiumAmountPerYear, firstCalculation.PremiumAmountPerYear * 1.15);
        }

        [Theory]
        [InlineData(3599, 1.05)]
        [InlineData(3600, 1.02)]
        [InlineData(4499, 1.02)]
        [InlineData(4500, 1)]
        void PostalcodePriceIsDifferent(int postal, double expectedPercentageDifference)
        {
            //Create data
            var firstHolder = new PolicyHolder(25, "01-01-2015", 8000, 0);
            var secondHolder = new PolicyHolder(25, "01-01-2015", postal, 0);
            var vehicle = new Vehicle(50, 1000, 2000);
            var coverage = InsuranceCoverage.WA;

            //Calculate
            var firstCalculation = new PremiumCalculation(vehicle, firstHolder, coverage);
            var secondCalculation = new PremiumCalculation(vehicle, secondHolder, coverage);

            //Check if price as expected
            Assert.Equal(firstCalculation.PremiumAmountPerYear * expectedPercentageDifference, secondCalculation.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(InsuranceCoverage.WA, 1)]
        [InlineData(InsuranceCoverage.WA_PLUS, 1.2)]
        [InlineData(InsuranceCoverage.ALL_RISK, 2)]
        void InsurancePricesAreDifferent(InsuranceCoverage insurance, double expectedPercentageDifference)
        {
            //Create data
            var holder = new PolicyHolder(25, "01-01-2015", 8000, 0);
            var vehicle = new Vehicle(50, 1000, 2000);
            var wa = InsuranceCoverage.WA;

            //Calculate
            var firstCalculation = new PremiumCalculation(vehicle, holder, wa);
            var secondCalculation = new PremiumCalculation(vehicle, holder, insurance);

            //Check if prices are as expected
            Assert.Equal(firstCalculation.PremiumAmountPerYear * expectedPercentageDifference, secondCalculation.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(5, 0)]
        [InlineData(6, .05)]
        [InlineData(18, .65)]
        [InlineData(19, .65)]
        public void DamageFreeYearsGiveRightDiscount(int years, double expectedDiscount)
        {
            //Create data
            var firstHolder = new PolicyHolder(25, "01-01-2015", 8000, 0);
            var secondHolder = new PolicyHolder(25, "01-01-2015", 8000, years);
            var vehicle = new Vehicle(50, 1000, 2000);
            var wa = InsuranceCoverage.WA;

            //Calculate
            var firstCalculation = new PremiumCalculation(vehicle, firstHolder, wa);
            var secondCalculation = new PremiumCalculation(vehicle, secondHolder, wa);

            //Check if discount is as expected
            var discount = firstCalculation.PremiumAmountPerYear * expectedDiscount;
            Assert.Equal(firstCalculation.PremiumAmountPerYear - discount, secondCalculation.PremiumAmountPerYear);
        }

        [Fact]
        public void YearPaymentGetsDiscount()
        {
            //Create data
            var holder = new PolicyHolder(25, "01-01-2015", 8000, 0);
            var vehicle = new Vehicle(50, 1000, 2000);
            var wa = InsuranceCoverage.WA;

            //Calculate
            var calculation = new PremiumCalculation(vehicle, holder, wa);

            //Check if discount is as expected
            Assert.Equal(Math.Round(calculation.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH) * 0.975, 2), calculation.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR)/12);
        }
    }
}
